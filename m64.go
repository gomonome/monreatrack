package monreatrack

import (
	"fmt"
	"sync"
	"time"

	"strings"

	"gitlab.com/gomonome/dawconnection"
	"gitlab.com/goosc/osc"
	"gitlab.com/goosc/reaper"
)

/*
Layout (monome 64)
 _______________
|S|S|S|S|S|S|S|S| <- Solo
|R|R|R|R|R|R|R|R| <- Rec Arm
|M|M|M|M|M|M|M|M| <- Mute
|+|+|+|+|+|+|+|+| <- volume up (always bright)
|-|-|-|-|-|-|-|-| <- volume down (always dark)
|L|L|L|L|L|L|L|L| <- latch automation active
|1|2|3|4|5|6|7|8| <- jump to marker 1-8
|r|p|u|a|b|c|m|t|
 ---------------

r: start/stop recording
p: start/pause playing
u: undo
m: metronome on/off
a: track bank a (1-8)
b: track bank b (9-16)
c: track bank c (17-24)
t: tap tempo

p is bright while playing
m is bright if metronome is on
active track bank is slowly blinking
*/

/*
Layout (monome 128)
 ______________________________________
|S|S|S|S|S|S|S|S|S|S |S |S |S |S |S |S | <- Solo
|R|R|R|R|R|R|R|R|R|R |R |R |R |R |R |R | <- Rec Arm
|M|M|M|M|M|M|M|M|M|M |M |M |M |M |M |M | <- Mute
|+|+|+|+|+|+|+|+|+|+ |+ |+ |+ |+ |+ |+ | <- volume up (always bright)
|-|-|-|-|-|-|-|-|-|- |- |- |- |- |- |- | <- volume down (always darker)
|L|L|L|L|L|L|L|L|L|L |L |L |L |L |L |L | <- latch automation active
|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16| <- jump to marker 1-16
|r|p|u|a|b|c|d|S|B|F||I |O |U |D |m |t |
 --------------------------------------

r: start/stop recording
p: start/pause playing
u: undo
a: track bank a (1-8)
b: track bank b (9-16)
c: track bank c (17-24)
d: track bank d (24-32)
m: metronome on/off
t: tap tempo
U: move track up (select track while U is pressed)
D: move track down (select track while D is pressed)
B: move playhead backward
F: move playhead forward
S: set marker at current playhead position
I: zoom in horizontal
O: zoom out horizontal

the pairs BF, IO, UD are such that the first is brighter and the second is darker
S is medium bright and slowly blinking
p is bright while playing
m is bright if metronome is on
active track bank is light other track banks are darker

*/

type m64Layout struct {
	cache         [8][8]bool
	last          [8][8]bool
	trackMuted    [24]bool
	trackRecArmed [24]bool
	trackSoloed   [24]bool
	trackLatched  [24]bool
	trackVol      [24]float32
	triggerer     dawconnection.Triggerer
	sync.RWMutex
	isPlaying bool
	trackBank uint8
}

// returns the track for the given y, taking into account the active bank
func (m *m64Layout) getTrack(y uint8) uint8 {
	m.RLock()
	bank := m.trackBank
	m.RUnlock()
	return (bank * 8) + y
}

func (m *m64Layout) soloTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	wasSoloed := m.trackSoloed[track]
	m.trackSoloed[track] = !wasSoloed
	m.Unlock()
	//	fmt.Printf("soloing track: %v %v\n", track, !wasSoloed)
	reaper.TRACK_SOLO.Set(track+1).WriteTo(m.triggerer, !wasSoloed)
}

func (m *m64Layout) recArmTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	wasArmed := m.trackRecArmed[track]
	m.trackRecArmed[track] = !wasArmed
	m.Unlock()
	//	fmt.Printf("recArming track: %v %v\n", track, !wasArmed)
	reaper.TRACK_REC_ARM.Set(track+1).WriteTo(m.triggerer, !wasArmed)
}

func (m *m64Layout) muteTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	wasMuted := m.trackMuted[track]
	m.trackMuted[track] = !wasMuted
	m.Unlock()
	//	fmt.Printf("muting track: %v %v\n", track, !wasMuted)
	reaper.TRACK_MUTE.Set(track+1).WriteTo(m.triggerer, !wasMuted)
}

func (m *m64Layout) latchAutoTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	wasLatched := m.trackLatched[track]
	m.trackLatched[track] = !wasLatched
	m.Unlock()
	//fmt.Printf("latching track: %v %v\n", track, !wasLatched)
	//	fmt.Printf("latching track: %v\n", track)
	//TRACK_AUTO_LATCH
	ReaperUnselectAllTracks(m.triggerer)
	time.Sleep(time.Millisecond * 5)
	reaper.TRACK_SELECT.Set(track+1).WriteTo(m.triggerer, true)
	time.Sleep(time.Millisecond * 5)
	if !wasLatched {
		ReaperAutoLatchSelectedTrack(m.triggerer)
	} else {
		ReaperReadAutoTrack(m.triggerer)
	}
	//	reaper.TRACK_AUTO_LATCH.WriteTo(m.triggerer)
}

func (m *m64Layout) volUpTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	m.trackVol[track] += 2
	if m.trackVol[track] > 0 {
		m.trackVol[track] = 0
	}
	vol := m.trackVol[track]
	m.Unlock()
	//	fmt.Printf("vol up track: %v\n", track)
	reaper.TRACK_VOLUME_f.Set(track+1).WriteTo(m.triggerer, vol)
}

func (m *m64Layout) volDownTrack(y uint8) {
	track := m.getTrack(y)
	m.Lock()
	m.trackVol[track] -= 2
	if m.trackVol[track] < -40 {
		m.trackVol[track] = -40
	}
	vol := m.trackVol[track]
	m.Unlock()
	//	fmt.Printf("vol down track: %v\n", track)
	reaper.TRACK_VOLUME_f.Set(track+1).WriteTo(m.triggerer, vol)
}

func (m *m64Layout) jumpToMarker(marker uint8) {
	//	fmt.Printf("jump to marker: %v\n", marker)
	reaper.GOTO_MARKER.Set(marker).WriteTo(m.triggerer)
}

func (m *m64Layout) Matches(path osc.Path) bool {
	if path == "/time" || path == "/time/str" || path == "/frames/str" || path == "/beat/str" || path == "/samples/str" || path == "/samples" {
		return false
	}
	return true
}

func (m *m64Layout) Set(x, y, brightness uint8) {
	m.Switch(x, y, brightness > 0)
}

func (m *m64Layout) Switch(x, y uint8, on bool) {
	m.Lock()
	m.cache[int(x)][int(y)] = on
	m.Unlock()
}

const (
	REC_BUTTON          uint8 = 0
	PLAY_BUTTON         uint8 = 1
	UNDO_BUTTON         uint8 = 2
	TRACK_BANK_A_BUTTON uint8 = 3
	TRACK_BANK_B_BUTTON uint8 = 4
	TRACK_BANK_C_BUTTON uint8 = 5
	METRONOME_BUTTON    uint8 = 6
	TEMPO_P_BUTTON      uint8 = 7
)

// Flush flushes the cache to the underlying device
func (m *m64Layout) Flush() {
	m.Lock()
	for x, mp := range m.cache {
		for y, on := range mp {
			if on != m.last[x][y] {
				m.triggerer.Switch(uint8(x), uint8(y), on)
			}
		}
	}
	m.last = m.cache
	m.Unlock()
}

func (p *m64Layout) blinkRunning() {
	var isPlaying bool

	for {
		p.RLock()
		isPlaying = p.isPlaying
		p.RUnlock()

		if isPlaying {
			p.Switch(7, PLAY_BUTTON, true)
			p.Flush()
			time.Sleep(time.Millisecond * 300)
		}
		p.Switch(7, PLAY_BUTTON, false)

		p.Flush()
		time.Sleep(time.Millisecond * 300)
	}
}

func (a *m64Layout) eachTrackMessage(path osc.Path, vals ...interface{}) {
	idx := strings.LastIndex(path.String(), "/")
	if idx < 0 {
		panic("must not happen")
	}
	suffix := path.String()[idx:]

	switch suffix {
	//	reaper said: /track/2/autolatch [1]
	case "/autolatch":
		var track int
		_, err := path.Scan("/track/%d/autolatch", &track)
		if err == nil {
			//			fmt.Printf("mute track %v to %v\n", track, reaper.HasAndIsOn(vals))
			a.Lock()
			a.trackLatched[track-1] = reaper.HasAndIsOn(vals)
			a.refreshTrackInfos()
			a.Unlock()
			//			a.Switch(2, uint8(reatrack-1), reaper.HasAndIsOn(vals))
		}
	case "/mute":
		var track int
		_, err := path.Scan("/track/%d/mute", &track)
		if err == nil {
			//			fmt.Printf("mute track %v to %v\n", track, reaper.HasAndIsOn(vals))
			a.Lock()
			a.trackMuted[track-1] = reaper.HasAndIsOn(vals)
			a.refreshTrackInfos()
			a.Unlock()
			//			a.Switch(2, uint8(reatrack-1), reaper.HasAndIsOn(vals))
		}
	case "/solo":
		var track int
		_, err := path.Scan("/track/%d/solo", &track)
		if err == nil {
			//track := a.getTrack(uint8(reatrack) - 1)
			//fmt.Printf("solo track %v to %v\n", track, reaper.HasAndIsOn(vals))
			a.Lock()
			a.trackSoloed[track-1] = reaper.HasAndIsOn(vals)
			a.refreshTrackInfos()
			a.Unlock()
			//a.Switch(0, uint8(reatrack-1), reaper.HasAndIsOn(vals))
		}
	case "/recarm":
		var track int
		_, err := path.Scan("/track/%d/recarm", &track)
		if err == nil {
			//track := a.getTrack(uint8(reatrack) - 1)
			fmt.Printf("recarm track %v to %v\n", track, reaper.HasAndIsOn(vals))
			a.Lock()
			a.trackRecArmed[track-1] = reaper.HasAndIsOn(vals)
			a.refreshTrackInfos()
			a.Unlock()
			//a.Switch(1, uint8(reatrack-1), reaper.HasAndIsOn(vals))
		}
	}
	a.Flush()
}

func (a *m64Layout) EachMessage(path osc.Path, vals ...interface{}) {
	//	fmt.Printf("reaper said: %s %v\n", path, vals)
	switch path.String() {
	//		[Record selected slot, go down (advanced)]
	/*
		case string(reaper.Autowrite):
			m.switchLightByVal(vals, 7, 4)
	*/
	case string(reaper.METRONOME):
		a.Switch(7, METRONOME_BUTTON, reaper.HasAndIsOn(vals))
		//		a.layout.SwitchActionButton(dawcontrol.METRONOME_BUTTON, reaper.HasAndIsOn(vals))
	case string(reaper.STOP):
		playing := !reaper.HasAndIsOn(vals)
		a.Lock()
		a.isPlaying = playing
		a.Unlock()
		a.Switch(7, PLAY_BUTTON, playing)
	case string(reaper.PLAY):
		playing := reaper.HasAndIsOn(vals)
		a.Lock()
		a.isPlaying = playing
		a.Unlock()
		a.Switch(7, PLAY_BUTTON, playing)
	case string(reaper.RECORD):
		recording := reaper.HasAndIsOn(vals)
		//		a.Lock()
		//		a.isPlaying = playing
		//		a.Unlock()
		a.Switch(7, REC_BUTTON, recording)
	default:
		if path.HasPrefix("/track") {
			a.eachTrackMessage(path, vals...)
		}
	}
	//	a.Flush()
}

func (m *m64Layout) Run() {
	reaper.DEVICE_TRACK_COUNT.Set(24).WriteTo(m.triggerer)
	//reaper.REAPER_TRACK_FOLLOWS_DEVICE.WriteTo(m.triggerer)
	//reaper.DEVICE_TRACK_FOLLOWS_DEVICE.WriteTo(m.triggerer)

	m.switchToTrackBank(0)

	// highlight volume up line
	m.Switch(3, 0, true)
	m.Switch(3, 1, true)
	m.Switch(3, 2, true)
	m.Switch(3, 3, true)
	m.Switch(3, 4, true)
	m.Switch(3, 5, true)
	m.Switch(3, 6, true)
	m.Switch(3, 7, true)
	m.Flush()
	go m.blinkRunning()
}

var _ dawconnection.Handler = &m64Layout{}

func (m *m64Layout) EachKeyPress(x, y uint8) {
	//	fmt.Printf("button pressed: %v/%v\n", x, y)
	//	defer m.Flush()
	switch x {
	case 0:
		m.soloTrack(y)
	case 1:
		m.recArmTrack(y)
	case 2:
		m.muteTrack(y)
	case 3:
		m.volUpTrack(y)
	case 4:
		m.volDownTrack(y)
	case 5:
		m.latchAutoTrack(y)
	case 6:
		m.jumpToMarker(y + 1)
	case 7:
		m.ActionPressed(y)
	}
	return
}

func (m *m64Layout) switchToTrackBank(bank uint8) {
	//	fmt.Printf("switchToTrackBank %v\n", bank)
	m.Lock()
	m.trackBank = bank
	m.cache[7][TRACK_BANK_A_BUTTON] = false
	m.cache[7][TRACK_BANK_B_BUTTON] = false
	m.cache[7][TRACK_BANK_C_BUTTON] = false
	m.cache[7][TRACK_BANK_A_BUTTON+bank] = true
	m.refreshTrackInfos()
	m.Unlock()
	m.Flush()
}

func (m *m64Layout) refreshTrackInfos() {
	for y := 0; y < 8; y++ {
		track := (int(m.trackBank) * 8) + y
		m.cache[0][y] = m.trackSoloed[track]
		m.cache[1][y] = m.trackRecArmed[track]
		m.cache[2][y] = m.trackMuted[track]
		m.cache[5][y] = m.trackLatched[track]
	}
}

func (m *m64Layout) ActionPressed(y uint8) {
	switch y {
	case REC_BUTTON:
		reaper.RECORD.WriteTo(m.triggerer)
	case PLAY_BUTTON:
		reaper.PLAY.WriteTo(m.triggerer)
	case UNDO_BUTTON:
		ReaperUndo(m.triggerer)
	case METRONOME_BUTTON:
		reaper.METRONOME.WriteTo(m.triggerer)
	case TEMPO_P_BUTTON:
		ReaperTempoTap(m.triggerer)
	case TRACK_BANK_A_BUTTON:
		m.switchToTrackBank(0)
	case TRACK_BANK_B_BUTTON:
		m.switchToTrackBank(1)
	case TRACK_BANK_C_BUTTON:
		m.switchToTrackBank(2)
	default:
		panic("unreachable")
	}

}

func (m *m64Layout) EachKeyRelease(x, y uint8) {
	//	fmt.Printf("button released: %v/%v\n", x, y)
	return
}

func (m *m64Layout) SetTriggerer(tr dawconnection.Triggerer) {
	m.triggerer = tr
}
