package monreatrack

import (
	"sync"
	"time"

	"gitlab.com/gomonome/dawconnection"
	"gitlab.com/goosc/osc"
)

const (
	m128ActiveClipBrightness        = 14
	m128FilledClipBrightness        = 8
	m128EmptySelectedClipBrightness = 3
	m128ActionButtonBrightness      = 12
)

type m128Layout struct {
	cache           [8][16]uint8
	last            [8][16]uint8
	filledClips     [7][14]bool
	activeTrackClip [7]uint8
	dawconnection.Triggerer
	sync.RWMutex
	//	actionButtons map[uint8]ActionButton
	//	actor         Actor
	//	blinker       *blinker128
}

func (m *m128Layout) Matches(path osc.Path) bool {
	if path == "/time" || path == "/time/str" || path == "/frames/str" || path == "/beat/str" || path == "/samples/str" || path == "/samples" {
		return false
	}
	return true
}

func (p *m128Layout) blinkSelected() {
	var hasSelectedClip bool
	var selectedClipTrack, selectedClipClip uint8
	var fillOrDeleteMode bool
	//	var showFilledMode bool

	for {
		//		fillOrDeleteMode = p.actor.InFillOrDeleteMode()
		//		showFilledMode = p.actor.InShowFilledMode()

		//if showFilledMode {
		p.BlankAllClips()
		p.PrintFilled()
		p.PrintActiveClips()
		//}

		p.Flush()

		if !fillOrDeleteMode {
			//			hasSelectedClip = p.actor.HasSelectedClip()
			if hasSelectedClip {
				//				selectedClipTrack, selectedClipClip = p.actor.SelectedClip()
				p.SwitchClip(selectedClipTrack, selectedClipClip, true)
				time.Sleep(time.Millisecond * 160)
				p.Flush()
				//p.m128.SwitchClip(selectedClipTrack, selectedClipClip, false)
				//time.Sleep(time.Millisecond * 100)
				//p.m128.SwitchClip(selectedClipTrack, selectedClipClip, true)
				//time.Sleep(time.Millisecond * 50)
				//p.m128.SwitchClip(selectedClipTrack, selectedClipClip, false)
				//				time.Sleep(time.Millisecond * 100)
			}
		}

		time.Sleep(time.Millisecond * 200)
	}
}

func (p *m128Layout) blinkRunning() {
	var isPlaying bool
	//	var showFilledMode bool

	for {
		//		isPlaying = p.actor.IsPlaying()
		//		showFilledMode = p.actor.InShowFilledMode()

		/*
			if !showFilledMode {
				p.m128.printActiveClips()
			}
		*/
		if isPlaying {
			//			p.SwitchActionButton(PLAY_BUTTON, true)
			time.Sleep(time.Millisecond * 300)
			//			p.SwitchActionButton(PLAY_BUTTON, false)
		} else {
			//			p.SwitchActionButton(PLAY_BUTTON, false)
		}

		p.Flush()
		time.Sleep(time.Millisecond * 300)
	}
}

func (m *m128Layout) NumClipsPerTrack() uint8 {
	return 14
}

func (m *m128Layout) Set(x, y, brightness uint8) {
	m.Lock()
	m.cache[int(x)][int(y)] = brightness
	m.Unlock()
}

func (m *m128Layout) Switch(x, y uint8, on bool) {
	var brightness uint8
	if on {
		brightness = 15
	}
	m.Set(x, y, brightness)
}

// Flush flushes the cache to the underlying device
func (m *m128Layout) Flush() {
	m.Lock()
	for x, mp := range m.cache {
		for y, brightness := range mp {
			if brightness != m.last[x][y] {
				//				m.triggerer.Set(uint8(x), uint8(y), brightness)
			}
		}
	}
	m.last = m.cache
	m.Unlock()
}

/*
func (m *m128Layout) SwitchActionButton(btn ActionButton, on bool) {
	var x uint8
	var found bool

	for _x, b := range m.actionButtons {
		if btn == b {
			found = true
			x = _x
		}
	}

	if found {
		var brightness uint8
		if on {
			brightness = m128ActionButtonBrightness
		}
		m.Set(x, 15, brightness)
	}
}
*/

func (m *m128Layout) BlankActiveTrackClips() {

	for tr, cl := range m.activeTrackClip {
		if cl > 0 {
			m.Switch(uint8(7-tr), uint8(cl-1), false)
		}
	}
	m.activeTrackClip = [7]uint8{}

}

// 0 = no active clip
func (m *m128Layout) ActiveTrackClip(track uint8) uint8 {
	m.RLock()
	var cl uint8 = m.activeTrackClip[int(track)]
	m.RUnlock()
	return cl
}

func (m *m128Layout) SetActiveTrackClip(track uint8, clip uint8) {
	m.Lock()
	m.activeTrackClip[int(track)] = clip
	m.Unlock()
	m.PrintActiveClips()
	m.Flush()
}

func (m *m128Layout) IsFilledClip(track uint8, clip uint8) bool {
	m.RLock()
	is := m.filledClips[int(track)][int(clip)]
	m.RUnlock()
	return is
}

func (m *m128Layout) FillClip(track uint8, clip uint8) {
	m.Lock()
	m.filledClips[int(track)][int(clip)] = true
	m.Unlock()
}

func (m *m128Layout) UnfillClip(track uint8, clip uint8) {
	m.Lock()
	m.filledClips[int(track)][int(clip)] = false
	m.Unlock()
}

// SwitchClip temporary disables the selected clip to get
// a blinking effect whithout changing the brightness
func (m *m128Layout) SwitchClip(track, clip uint8, on bool) {
	//	m.triggerer.Switch(7-track, clip, false)
	m.Lock()
	// to trigger the display for the next flush
	m.last[7-uint8(track)][uint8(clip)] = 0
	if m.cache[7-uint8(track)][uint8(clip)] == 0 {
		m.cache[7-uint8(track)][uint8(clip)] = m128EmptySelectedClipBrightness
	}
	m.Unlock()
}

func (m *m128Layout) PrintActiveClips() {
	m.RLock()
	activeTrackClip := m.activeTrackClip
	m.RUnlock()

	for tr, cl := range activeTrackClip {
		if cl > 0 {
			//m.Switch(uint8(7-tr), cl-1, true)
			m.Set(uint8(7-tr), cl-1, m128ActiveClipBrightness)
		}
	}
	/*
		time.Sleep(time.Millisecond * 200)

		for y, cl := range activeTrackClip {
			if cl > 0 {
				m.Switch(cl-1, uint8(y), false)
			}
		}
	*/

}

func (m *m128Layout) PrintFilled() {
	m.BlankAllClips()
	for tr, cl := range m.filledClips {
		for c, filled := range cl {
			if filled {
				m.Set(uint8(7-tr), uint8(c), m128FilledClipBrightness)
			}
		}
	}
}

/*
func (m *m128Layout) setActor(act Actor) {
	m.actor = act
}
*/

func (m *m128Layout) Run() {
	/*
		m.actionButtons = map[uint8]ActionButton{
			7: TRIGGER_BUTTON,
			6: PLAY_BUTTON,
			5: UNDO_BUTTON,
			4: DELETE_BUTTON,
			3: AUTOMATION_BUTTON,
			2: OVERDUB_BUTTON,
			1: METRONOME_BUTTON,
			0: TEMPO_TAP_BUTTON,
		}
	*/
	//act := &actor{Mapper: m, playtime: playtime.New(7, 14, m), reaperAction: &reaperAction{m}}
	//	m.blinker = &blinker128{m, act}
	go m.blinkRunning()
	go m.blinkSelected()
	//	go m.filledChecked()
}

func (m *m128Layout) NumTracks() uint8 {
	return 7
}

//var _ layoutHandler = &m128Layout{}

func (m *m128Layout) SwitchTrack(track uint8, on bool) {
	if track < 7 {
		//fmt.Printf("switching track %d on\n", track)
		m.Switch(7-track, 14, on)
	}
	m.Flush()
}

func (m *m128Layout) EachKeyPress(x, y uint8) {
	//	fmt.Printf("pressed: %d/%d\n", x, y)
	defer m.Flush()
	switch y {
	case 15:
		//		m.actor.ActionPressed(m.actionButtons[x])
		return
	case 14:
		if x == 0 {
			//			m.actor.StopAllClips()
			return
		} else {
			//			m.actor.TrackStop(7 - x)
			m.Switch(x, y, true)
		}
		return
	}

	switch x {
	case 0:
		//		switchOn := m.actor.ScenePressed(y)
		//		m.Switch(x, y, switchOn)
		return
	default:
		//		m.actor.ClipPressed(7-x, y)
		//m.Actor.ClipPressed(y, x)
	}
}

func (m *m128Layout) BlankAllClips() {
	for x := uint8(7); x > 0; x-- {
		for y := uint8(0); y < 14; y++ {
			m.Switch(x, y, false)
		}
	}
}

func (m *m128Layout) EachMessage(path osc.Path, vals ...interface{}) {
	//	fmt.Printf("got message: %s %v\n", path.String(), vals)
	//	m.actor.EachMessage(path, vals...)
	m.Flush()
}

func (m *m128Layout) EachKeyRelease(x, y uint8) {
	//	fmt.Printf("released: %d/%d\n", x, y)
	defer m.Flush()
	if y == 15 {
		//		m.actor.ActionReleased(m.actionButtons[x])
		return
	}

	if x == 0 {
		m.Switch(x, y, false)
	}

	if y == 14 {
		m.Switch(x, y, false)
		return
	}

	//	if !m.actor.InShowFilledMode() {
	m.Switch(x, y, false)
	//	}
}

func (m *m128Layout) SetTriggerer(tr dawconnection.Triggerer) {
	//	m.triggerer = tr
}
