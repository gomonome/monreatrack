package monreatrack

import (
	"io"

	"gitlab.com/goosc/reaper"
)

func ReaperTempoTap(wr io.Writer) error {
	var action int32 = 1134
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func ReaperUndo(wr io.Writer) error {
	var action int32 = 40029
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func ReaperAutoLatchSelectedTrack(wr io.Writer) error {
	var action int32 = 40404
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func ReaperReadAutoTrack(wr io.Writer) error {
	var action int32 = 40400
	return reaper.ACTION.Set(action).WriteTo(wr)
}

func ReaperUnselectAllTracks(wr io.Writer) error {
	var action int32 = 40297
	return reaper.ACTION.Set(action).WriteTo(wr)
}
