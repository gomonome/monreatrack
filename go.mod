module gitlab.com/gomonome/monreatrack

require (
	gitlab.com/gomonome/dawconnection v0.2.0
	gitlab.com/gomonome/monome v0.4.0
	gitlab.com/goosc/osc v0.2.0
	gitlab.com/goosc/reaper v0.3.0
	gitlab.com/metakeule/config v1.13.0
)
