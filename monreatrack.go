package monreatrack

import (
	"gitlab.com/gomonome/dawconnection"
	"gitlab.com/gomonome/monome"
)

func New() dawconnection.Connection {
	return dawconnection.New("monreatrack", findHandler)
}

func findHandler(conn monome.Connection) dawconnection.Handler {
	switch conn.Rows() * conn.Cols() {
	case 64:
		return &m64Layout{}
		//	case 128:
		//	return &m128Layout{}
	}
	return nil
}
