package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/gomonome/monome"
	"gitlab.com/gomonome/monreatrack"
	"gitlab.com/goosc/reaper"
	"gitlab.com/metakeule/config"
)

var (
	cfg    = config.MustNew("monreatrack", "0.0.1", "control reaper tracks with monome")
	inAddr = cfg.NewString("in", "address where reaper sends to and monome should receive from.",
		config.Default("127.0.0.1:9001"))
	outAddr = cfg.NewString("out", "address where reaper receives from and monome should sends to.",
		config.Default("127.0.0.1:8001"))
)

func run() error {
	err := cfg.Run()
	if err != nil {
		return err
	}

	var sigchan = make(chan os.Signal, 10)

	m := monreatrack.New()
	err = m.Connect(reaper.New(m, reaper.ListenAddress(inAddr.Get()), reaper.WriteAddress(outAddr.Get())), func(err error) {
		sigchan <- os.Interrupt
	})

	if err != nil {
		return err
	}

	fmt.Fprintf(os.Stdout, "listening on %v and writing to %v\n", inAddr.Get(), outAddr.Get())

	m.Run()

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted, cleaning up...")
	monome.SwitchAll(m, false)
	m.Close()
	fmt.Fprintln(os.Stdout, "done")
	return nil
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
	os.Exit(0)
}
